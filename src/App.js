import React from 'react'
import 'antd/dist/antd.css'
import './styles/main.scss'
import { Router } from 'react-router-dom'
import { history } from 'stores/routing'
import Routes from 'routes/setup'

function App() {
  return (
    <Router history={history}>
      <Routes />
    </Router>
  )
}

export default App
