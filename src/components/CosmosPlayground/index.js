import React from 'react'
import s from './style.module.scss'

function CosmosPlayground({ children, background }) {
  return (
    <div className={`${s.playground} ${background === 'white' ? s.white : ''}`}>
      {children}
    </div>
  )
}

export function CosmosPlaygroundEmpty({ children }) {
  return (
    <div className={s.white}>
      {children}
    </div>
  )
}

export default CosmosPlayground
