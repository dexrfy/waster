import React from 'react'
import { CosmosPlayground } from 'components'
import DishDetails from '../index'

const DishDetailsCosmos = (props) => (
  <CosmosPlayground>
    <DishDetails {...props} />
  </CosmosPlayground>
)

DishDetailsCosmos.displayName = 'DishDetails'

export default {
  component: DishDetailsCosmos,

  name: 'primary',
  props: {
    type: 'primary',
  },
}
