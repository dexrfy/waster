import React from 'react'
import { Tag, Typography, Button, Icon } from 'antd'
import s1 from 'assets/images/dishes/1.jpg'
import s from './style.module.scss'

const { Title, Text } = Typography

function DishDetails() {
  return (
    <div className={s.section}>
      <img className={s.img} alt="example" src={s1} />
      <div className={s.block}>
        <span className={s.tags}>
          <Tag color="lime">vegan</Tag>
          <Tag color="green">no gluten</Tag>
        </span>
        <Title className={s.title} level={2}>
          Salad 1
        </Title>
        <Text className={s.paragraph}>
          Warm oat milk tofu with girolles & white asparagus. Baked cauliflower mushroom, summer
          vegetables & lemon thyme. Green olive & olive oil sorbet.
        </Text>
        <div className={`${s.price} ${s.text}`}>Price: $15</div>
        <div className={s.row}>
          <div className={s.text}>SELECT PORTIONS</div>
          <span className={s.row}>
            <Icon type="minus-circle" />
            <div className={s.text}>2</div>
            <Icon type="plus-circle" />
          </span>
        </div>
        <div className={s.row}>
          <div className={s.text}>TO PAY:</div>
          <div className={s.text}>30$</div>
        </div>
        <Button className={s.btn} type="primary" block>
          ADD TO MY ORDER
        </Button>
      </div>
    </div>
  )
}

export default DishDetails
