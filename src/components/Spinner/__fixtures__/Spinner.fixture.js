import Spinner from '../index'

export default [
  {
    component: Spinner,
    name: 'small',
    props: {
      size: 30,
      color: '#c6a676',
    },
  },
  {
    component: Spinner,
    name: 'normal',
    props: {
      size: 60,
      color: '#c6a676',
    },
  },
]
