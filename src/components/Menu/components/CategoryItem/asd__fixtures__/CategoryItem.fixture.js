import React from 'react'
import { CosmosPlayground } from 'components'
import Component from '../index'


const CategoryItemFixture = (props) => (
  <CosmosPlayground>
    <Component {...props} />
  </CosmosPlayground>
)

CategoryItemFixture.displayName = 'CategoryItem'

export default {
  component: CategoryItemFixture,

  name: 'default',
  props: {
    categoryName: 'VEGAN',
  },
}
