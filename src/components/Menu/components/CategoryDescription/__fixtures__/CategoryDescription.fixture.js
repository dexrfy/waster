import React from 'react'
import { CosmosPlaygroundEmpty } from 'components'
import Component from '../index'


const Fixture = (props) => (
  <CosmosPlaygroundEmpty >
    <Component {...props} />
  </CosmosPlaygroundEmpty>
)

Fixture.displayName = 'MenuDescription'

export default {
  component: Fixture,

  name: 'description',
  props: {
    type: 'default',
  },
}
