import React from 'react'
import s from './style.module.scss'

function CategoryDescription() {
  return (
    <div className={s.wrapper}>
      <div className={s.caption}>TASTING MENU</div>
      <div className={s.description}>
      Description
        {' '}
        <br />
        <br />
      Available: Lunch and dinner, Monday — Sunday
      </div>
    </div>
  )
}

export default CategoryDescription
