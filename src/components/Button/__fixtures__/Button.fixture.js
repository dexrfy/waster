import React from 'react'
import { Button } from 'antd'

const Btn = (props) => (
  <Button {...props}>Example</Button>
)

Btn.displayName = 'Button'

export default [{
  component: Btn,
  name: 'primary',
  props: {
    type: 'primary',
  },
},
{
  component: Btn,
  name: 'default',
  props: {
  },
},
{
  component: Btn,
  name: 'danger',
  props: {
    type: 'danger',
  },
},
{
  component: Btn,
  name: 'link',
  props: {
    type: 'link',
  },
}]
