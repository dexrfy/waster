import React from 'react'
import s from './style.module.scss'

function WelcomeButton() {
  return (
    <div className={s.button}>
      <p className={s.text}>START ORDERING</p>
    </div>
  )
}

export default WelcomeButton
