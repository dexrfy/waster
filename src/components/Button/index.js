import React from 'react'
import { WelcomeButton } from './components'

function Button({ type }) {
  if (type === 'welcome') return <WelcomeButton />
  return <div>Default Button</div>
}

export default Button
