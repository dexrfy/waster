const menu = {
  salePoint: {
    name: 'Waster',
    image: 'assets/images/welcome.png',
  },
  menu: {
    section: [{ id: 1, name: 'Food' }, { id: 2, name: 'Drinks' }],
    tags: [{ id: 1, name: 'no gluten' }, { id: 2, name: 'vegan' }],
    categories: [
      { id: 1, sectionId: 1, name: 'Salads' },
      { id: 2, sectionId: 1, name: 'Starters' },
      { id: 3, sectionId: 1, name: 'Breakfast' },
      { id: 4, sectionId: 1, name: 'Lunch' },
      { id: 5, sectionId: 1, name: 'Dinner' },
      { id: 6, sectionId: 1, name: 'Soup' },
      { id: 7, sectionId: 1, name: 'Desserts' },
      { id: 8, sectionId: 2, name: 'Coffee' },
      { id: 9, sectionId: 2, name: 'Beer' },
      { id: 10, sectionId: 2, name: 'Сocktails' },
    ],
    products: [
      {
        id: 1,
        title: 'Salad 1',
        image: 'assets/images/dishes/1.jpg',
        price: 12.0,
        categoryId: 1,
        description: 'Food description placeholder...',
        tags: [1, 2],
      },
      {
        id: 2,
        title: 'Salad 2',
        image: 'assets/images/dishes/2.jpg',
        price: 9.0,
        categoryId: 1,
        description: 'Food description placeholder...',
        tags: [2],
      },
      {
        id: 3,
        title: 'Salad 3',
        image: 'assets/images/dishes/3.jpg',
        price: 23.0,
        categoryId: 1,
        description: 'Food description placeholder...',
      },
      {
        id: 4,
        title: 'Salad 4',
        image: 'assets/images/dishes/4.jpg',
        price: 11.0,
        categoryId: 1,
        description: 'Food description placeholder...',
      },
    ],
  },
}

export default menu
