import React, { Suspense, lazy } from 'react'
import { Switch } from 'react-router-dom'
import { Spinner } from 'components'
import { App, Error, Empty } from 'layouts'
import NotFound from 'pages/Error'

import {
  ERROR, WELCOME_SCREEN,
  MENU, MENU_CATEGORY,
  DISH
} from './index'

const WelcomeScreen = lazy(() => import('pages/WelcomeScreen'))
const Menu = lazy(() => import('pages/Menu'))
const MenuCategory = lazy(() => import('pages/MenuCategory'))
const DishInfo = lazy(() => import('pages/DishInfo'))

export default function Routes() {
  return (
    <Switch>
      <Empty exact path={WELCOME_SCREEN} render={LazyComponent(WelcomeScreen)} />
      <App exact path={MENU} render={LazyComponent(Menu)} />
      <App exact path={MENU_CATEGORY} render={LazyComponent(MenuCategory)} />
      <App exact path={DISH} render={LazyComponent(DishInfo)} />

      <Error path={ERROR} component={NotFound} />
      <Error component={NotFound} />
    </Switch>
  )
}

function LazyComponent(Component) {
  return (props) => (
    <Suspense fallback={<Spinner />}>
      <Component {...props} />
    </Suspense>
  )
}
