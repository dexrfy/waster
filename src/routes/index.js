export const ERROR = '/404'

// routes need auth
export const HOME = '/home'
export const CALENDAR_WEEKLY = '/week'

export const WELCOME_SCREEN = '/'
export const MENU = '/menu'
export const MENU_CATEGORY = '/menu/category'
export const ORDER = '/order'

export const DISH = '/dish'
