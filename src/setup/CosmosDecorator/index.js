import React from 'react'
import 'antd/dist/antd.css'
import 'styles/main.scss'
import s from './style.module.scss'

function CosmosDecorator({ children }) {
  return (
    <div className={s.decorator}>{children}</div>
  )
}


export default CosmosDecorator
