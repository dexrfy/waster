import React from 'react'
import { Header } from '../components'
import s from './style.module.scss'

const AppLayout = ({ children }) => (
  <>
    <Header />
    <div className="contentWrapper">
      <main className={s.content}>{children}</main>
      {/* <Footer /> */}
    </div>
  </>
)

export default AppLayout
