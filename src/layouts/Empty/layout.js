import React from 'react'
import s from './style.module.scss'

const Empty = ({ children }) => <main className={s.wrapper}>{children}</main>

export default Empty
