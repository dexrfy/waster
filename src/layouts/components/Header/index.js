import React from 'react'
import { observer } from 'mobx-react-lite'
import { PageHeader } from 'antd'
import s from './style.module.scss'
// import { SIGN_IN } from 'routes/index'

const Header = observer(() => (
  <header className={s.header}>
    <PageHeader className={s.pageHeader} title="Menu" />
  </header>
))

export default Header
