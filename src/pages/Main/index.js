import React from 'react'
import { observer } from 'mobx-react-lite'
import { Button } from 'antd'
import welcome from 'assets/images/welcome.png'
import s from './style.module.scss'

const Main = observer(() => (
  <div className={s.wrapper}>
    <div className={s.logoWrapper}>
      <img className={s.logo} src={welcome} alt="" />
      <h1 className={s.name}>Waster</h1>
    </div>
    <div className={s.btnWrapper}>
      <Button size="large" className="">Start ordering</Button>
    </div>
  </div>
))

export default Main
