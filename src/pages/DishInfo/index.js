import React from 'react'
import { DishDetails } from 'components'
import s from './style.module.scss'

const DishInfo = () => (
  <div className={s.wrapper}>
    <DishDetails />
  </div>
)

export default DishInfo
