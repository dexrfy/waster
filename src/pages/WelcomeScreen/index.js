import React from 'react'
import { observer } from 'mobx-react-lite'
import { Button } from 'antd'
import welcome from 'assets/images/welcome.png'
import { Link } from 'react-router-dom'
import { MENU } from 'routes'
import s from './style.module.scss'

const Welcome = observer(() => (
  <div className={s.wrapper}>
    <div className={s.logoWrapper}>
      <img className={s.logo} src={welcome} alt="" />
      <h1 className={s.name}>Waster</h1>
    </div>
    <div className={s.btnWrapper}>
      <Link to={MENU}><Button size="large" className="">Start ordering</Button></Link>
    </div>
  </div>
))

export default Welcome
