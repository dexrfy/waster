import React from 'react'
import { observer } from 'mobx-react-lite'
import { Menu, Icon } from 'antd'
import { history } from 'stores/routing'
import { MENU_CATEGORY } from 'routes/index'

const { SubMenu } = Menu

const Main = observer(() => (
  <div >
    <Menu
      defaultOpenKeys={['sub1', 'sub2']}
      mode="inline"
      onClick={() => history.push(MENU_CATEGORY)}
    >
      <SubMenu
        key="sub1"
        title={(
          <span>
            <Icon type="mail" />
            <span>Food</span>
          </span>
      )}
      >
        <Menu.Item key="1">Salads</Menu.Item>
        <Menu.Item key="2">Starters</Menu.Item>
        <Menu.Item key="3">Breakfast</Menu.Item>
        <Menu.Item key="4">Lunch</Menu.Item>
        <Menu.Item key="5">Dinner</Menu.Item>
        <Menu.Item key="6">Soup</Menu.Item>
        <Menu.Item key="7">Desserts</Menu.Item>
      </SubMenu>
      <SubMenu
        key="sub2"
        title={(
          <span>
            <Icon type="mail" />
            <span>Drinks</span>
          </span>
        )}
      >
        <Menu.Item key="8">Coffee</Menu.Item>
        <Menu.Item key="10">Beer</Menu.Item>
        <Menu.Item key="11">Сocktails</Menu.Item>
      </SubMenu>
    </Menu>
  </div>
))

export default Main
