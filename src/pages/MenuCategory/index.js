import React from 'react'
import { observer } from 'mobx-react-lite'
import { Card, Icon } from 'antd'
import { Link } from 'react-router-dom'
import s1 from 'assets/images/dishes/1.jpg'
import s2 from 'assets/images/dishes/2.jpg'
import s3 from 'assets/images/dishes/3.jpg'
import s4 from 'assets/images/dishes/4.jpg'
import s from './style.module.scss'

const MenuCategory = observer(() => (
  <div className={s.category}>
    <Card
      title="Salad 1"
      extra={<Icon type="plus-circle" />}
      cover={<img alt="example" src={s1} />}
      actions={[
        <Icon type="shopping-cart" />,
        <Link to="/dish">More</Link>,
        <div>$12.00</div>,
      ]}
    />
    <Card
      title="Salad 2"
      extra={<Icon type="plus-circle" />}
      cover={<img alt="example" src={s2} />}
      actions={[
        <Icon type="shopping-cart" />,
        <Link to="/dish">More</Link>,
        <div>$9.00</div>,
      ]}
    />
    <Card
      title="Salad 3"
      extra={<Icon type="plus-circle" />}
      cover={<img alt="example" src={s3} />}
      actions={[
        <Icon type="shopping-cart" />,
        <Link to="/dish">More</Link>,
        <div>$23.00</div>,
      ]}
    />
    <Card
      title="Salad 4"
      extra={<Icon type="plus-circle" />}
      cover={<img alt="example" src={s4} />}
      actions={[
        <Icon type="shopping-cart" />,
        <Link to="/dish">More</Link>,
        <div>$11.00</div>,
      ]}
    />
  </div>
))

export default MenuCategory
